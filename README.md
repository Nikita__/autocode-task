### Описание
Ученые проводили эксперименты с колонией микроорганизмов.
Известно, что за каждые 30 минут масса колонии увеличивается в 3 раза. Программа получает на вход начальную массу колонии и массив, в котором записаны продолжительности всех экспериментов в минутах.

### Детали

- Все входные данные - целые числа
- Промежуточные результаты нужно округлить до ближайшего целого числа
- Длительность одного эксперимента не меньше 30 минут


### Пример

Масса: 13 (мг), Эксперименты [90] (минут)
Результат: 351 (мг)
